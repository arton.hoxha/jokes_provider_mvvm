import 'package:flutter/foundation.dart';
import 'package:jokes/business_logic/model/joke.dart';
import 'package:jokes/services/repository/joke_repository.dart';

enum FavoriteStatus { loading, success, failure }

class FavoriteViewModel extends ChangeNotifier {
  List<Joke> _jokes = [];
  FavoriteStatus status = FavoriteStatus.loading;

  final JokeRepository jokeRepository;

  FavoriteViewModel({required this.jokeRepository});

  List<Joke> get favorites => _jokes;

  void loadData() async {
    _jokes = await jokeRepository.getFavorites();
    status = FavoriteStatus.success;
    notifyListeners();
  }
}
