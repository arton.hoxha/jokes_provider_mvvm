import 'package:jokes/business_logic/model/joke.dart';

abstract class JokeApi {
  Future<Joke> getJoke(bool safe);
}
