class Joke {
  int id;
  String category;
  String joke;
  bool safe;
  bool favorite;

  Joke(
      {required this.id,
      required this.category,
      required this.joke,
      required this.safe,
      required this.favorite});

  Joke.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        category = json['category'],
        joke = json['joke'],
        safe = json['safe'],
        favorite = false;

  Map<String, dynamic> toJson() => {
        'id': id,
        'category': category,
        'joke': joke,
        'safe': safe,
        'favorite': favorite,
      };

  Map<String, dynamic> toSqlite() => {
        'id': id,
        'category': category,
        'joke': joke,
        'safe': safe ? 1 : 0,
        'favorite': favorite ? 1 : 0
      };

  Joke.fromSqlite(Map<String, dynamic> data)
      : id = data['id'],
        category = data['category'],
        joke = data['joke'],
        safe = data['safe'] > 0 ? true : false,
        favorite = data['favorite'] > 0 ? true : false;

  static final empty = Joke(
      id: -1, category: "unkown", joke: "--", safe: false, favorite: false);

  @override
  String toString() {
    // TODO: implement toString
    return "{id: ${id},category: ${category},joke: joke,safe: ${safe},favorite: ${favorite}}";
  }
}
