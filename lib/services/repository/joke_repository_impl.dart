import 'package:jokes/services/joke_storage/joke_storage.dart';
import 'package:jokes/services/joke_storage/joke_storage_impl.dart';

import '../../business_logic/model/joke.dart';
import '../joke_api/joke_api.dart';
import 'joke_repository.dart';

class JokeRepositoryImpl implements JokeRepository {
  final JokeApi jokeApi;
  final JokeStorage jokeStorage;

  JokeRepositoryImpl({required this.jokeApi, required this.jokeStorage});

  @override
  Future<Joke> getJoke(bool isSafeMode) async {
    final joke = await jokeApi.getJoke(isSafeMode);
    return joke;
  }

  @override
  Future<List<Joke>> getFavorites() async {
    return await jokeStorage.getFavorites();
  }

  @override
  Future<bool> editFavorite(Joke joke) async {
    if (joke.favorite) {
      final res = await jokeStorage.saveFavorite(joke);
      return res > 0;
    } else {
      final res = await jokeStorage.deleteFavorite(joke);
      return res > 0;
    }
  }
}
