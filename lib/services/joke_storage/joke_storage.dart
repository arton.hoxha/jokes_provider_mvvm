import 'package:jokes/business_logic/model/joke.dart';

abstract class JokeStorage {
  Future<List<Joke>> getFavorites();
  Future<int> saveFavorite(Joke joke);
  Future<int> deleteFavorite(Joke joke);
}
