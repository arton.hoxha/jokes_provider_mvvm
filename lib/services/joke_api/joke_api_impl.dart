import 'dart:convert';

import 'package:jokes/business_logic/model/joke.dart';
import 'package:jokes/services/joke_api/joke_api.dart';
import 'package:http/http.dart' as http;

class JokeRequestFailure implements Exception {}

class JokeNotFoundFailure implements Exception {}

class JokeApiImpl implements JokeApi {
  JokeApiImpl({http.Client? httpClient})
      : _httpClient = httpClient ?? http.Client();

  final http.Client _httpClient;
  static const _baseUrl = 'v2.jokeapi.dev';

  //"https://v2.jokeapi.dev/joke/Any?type=single" &safe-mode

  @override
  Future<Joke> getJoke(bool safe) async {
    final jokeRequest = Uri.https(
        _baseUrl, '/joke/Any', {'type': 'single', safe ? 'safe-mode' : '': ''});
    final jokeResponse = await _httpClient.get(jokeRequest);

    if (jokeResponse.statusCode != 200) {
      throw JokeRequestFailure();
    }

    final jokeJson = jsonDecode(jokeResponse.body) as Map<String, dynamic>;

    if (jokeJson.isEmpty) {
      throw JokeNotFoundFailure();
    }

    return Joke.fromJson(jokeJson);
  }
}
