import '../../business_logic/model/joke.dart';

abstract class JokeRepository {
  Future<List<Joke>> getFavorites();
  Future<bool> editFavorite(Joke joke);
  Future<Joke> getJoke(bool isSafeMode);
}
