import 'dart:math';

import 'package:jokes/business_logic/model/joke.dart';
import 'package:jokes/services/repository/joke_repository.dart';

class JokeRepositoryFake implements JokeRepository {
  @override
  Future<List<Joke>> getFavorites() async {
    return [
      Joke(
          id: 123,
          category: "programming",
          joke: "Programming is a joke",
          safe: true,
          favorite: true),
      Joke(
          id: 123,
          category: "programming",
          joke: "Programming is a joke",
          safe: true,
          favorite: true),
      Joke(
          id: 123,
          category: "programming",
          joke: "Programming is a joke",
          safe: true,
          favorite: true),
    ];
  }

  @override
  Future<Joke> getJoke(bool isSafeMode) async {
    return Future.delayed(
        const Duration(seconds: 1), () => _jokes[rand.nextInt(_jokes.length)]);
  }

  final rand = Random();

  final List<Joke> _jokes = [
    Joke(
        id: 123,
        category: "programming",
        joke: "Programming is a joke",
        safe: true,
        favorite: false),
    Joke(
        id: 123,
        category: "misc",
        joke: "This is a test, oh no.",
        safe: true,
        favorite: false),
    Joke(
        id: 123,
        category: "programming",
        joke:
            "There are 10 people in the world. Those who understand binary and those who don't.",
        safe: true,
        favorite: false)
  ];

  @override
  Future<bool> editFavorite(Joke joke) async {
    //TODO: Check in DB if it exists
    return true;
  }
}
