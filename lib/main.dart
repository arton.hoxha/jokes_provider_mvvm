import 'package:flutter/material.dart';
import 'package:jokes/business_logic/view_models/joke_viewmodel.dart';
import 'package:jokes/services/joke_api/joke_api_impl.dart';
import 'package:jokes/services/joke_storage/joke_storage_impl.dart';
import 'package:jokes/services/repository/joke_repository.dart';
import 'package:jokes/services/repository/joke_repository_impl.dart';
import 'package:jokes/ui/views/favorites/favorite_view.dart';
import 'package:jokes/ui/views/joke/joke_view.dart';
import 'package:provider/provider.dart';

import 'business_logic/view_models/favorite_viewmodel.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  runApp(MyApp(
    jokeRepository: JokeRepositoryImpl(
        jokeApi: JokeApiImpl(), jokeStorage: JokeStorageImpl()),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key, required this.jokeRepository}) : super(key: key);

  final JokeRepository jokeRepository;

  @override
  Widget build(BuildContext context) {
    return Provider.value(
        value: (context) => jokeRepository,
        child: MultiProvider(
          providers: [
            ChangeNotifierProvider(
              create: (_) => FavoriteViewModel(jokeRepository: jokeRepository),
            ),
            ChangeNotifierProvider(
                create: (_) => JokeViewModel(jokeRepository: jokeRepository))
          ],
          child: MaterialApp(
            theme: ThemeData(
                primarySwatch: Colors.blue,
                visualDensity: VisualDensity.adaptivePlatformDensity),
            home: const FavoriteView(),
          ),
        ));
  }
}
