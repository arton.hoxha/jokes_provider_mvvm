import 'package:flutter/material.dart';
import 'package:jokes/business_logic/view_models/favorite_viewmodel.dart';
import 'package:jokes/main.dart';
import 'package:jokes/business_logic/model/joke.dart';
import 'package:jokes/ui/views/favorites/widgets/favorite_empty.dart';
import 'package:jokes/ui/views/joke/joke_view.dart';
import 'package:provider/provider.dart';

import '../../widgets/jokes_scaffold.dart';

class FavoriteView extends StatefulWidget {
  const FavoriteView({Key? key});

  @override
  State<FavoriteView> createState() => _FavoriteViewState();
}

class _FavoriteViewState extends State<FavoriteView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final favoriteViewModel = context.watch<FavoriteViewModel>();

    switch (favoriteViewModel.status) {
      case FavoriteStatus.loading:
        favoriteViewModel.loadData();
        return const JokesScaffold(
            body: Center(
          child: CircularProgressIndicator(),
        ));
      case FavoriteStatus.success:
        if (favoriteViewModel.favorites.isEmpty) {
          return const JokesScaffold(body: FavoriteEmpty());
        }

        return JokesScaffold(body: buildListView(favoriteViewModel.favorites));
      case FavoriteStatus.failure:
        return const JokesScaffold(
            body: Center(
          child: Text("Error getting favorites"),
        ));
    }
  }

  ListView buildListView(List<Joke> jokes) {
    return ListView.builder(
        itemCount: jokes.length,
        itemBuilder: (context, index) {
          return ListTile(
            key: ValueKey("${jokes[index].id}$index"),
            onTap: () async {
              final rebuild = await Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (_) => JokeView(
                            mode: true,
                            position: index,
                          )));
              if (rebuild) {
                Provider.of<FavoriteViewModel>(context, listen: false)
                    .loadData();
              }
            },
            title: Text(jokes[index].joke),
            subtitle: Text(jokes[index].category),
          );
        });
  }
}
