import 'package:flutter/material.dart';

class JokeError extends StatelessWidget {
  const JokeError({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: Text("Error fetching joke"),
    );
  }
}
