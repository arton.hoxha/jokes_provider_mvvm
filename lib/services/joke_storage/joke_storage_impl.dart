import 'package:jokes/business_logic/model/joke.dart';
import 'package:jokes/services/joke_storage/joke_storage.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class JokeStorageImpl extends JokeStorage {
  @override
  Future<List<Joke>> getFavorites() async {
    final JokeDbHelper dbHelper = JokeDbHelper();
    if (!dbHelper.isInitialized) {
      await dbHelper.initDb();
    }
    return await dbHelper.getJokes();
  }

  @override
  Future<int> saveFavorite(Joke joke) async {
    final JokeDbHelper dbHelper = JokeDbHelper();
    //await dbHelper.initDb();
    return await dbHelper.insert(joke);
  }

  @override
  Future<int> deleteFavorite(Joke joke) async {
    final JokeDbHelper dbHelper = JokeDbHelper();
    return await dbHelper.remove(joke);
  }
}

class JokeDbHelper {
  final String tableJoke = 'jokes';
  final String dbName = 'jokes.db';

  static JokeDbHelper? _instance;
  JokeDbHelper._() {}

  Database? db;

  factory JokeDbHelper() {
    _instance ??= JokeDbHelper._();
    return _instance!;
  }

  bool get isInitialized => db != null;

  Future<void> initDb() async {
    db = await openDatabase(join(await getDatabasesPath(), dbName),
        onCreate: (db, version) async {
      return await db.execute(
          'CREATE TABLE $tableJoke(id INTEGER PRIMARY KEY, joke TEXT, category TEXT, safe INTEGER, favorite INTEGER)');
    }, version: 1);
  }

  Future<bool> update(Joke joke) async {
    return await db!.update(tableJoke, joke.toSqlite()) > 0;
  }

  Future<int> insert(Joke joke) async {
    return await db!.insert(tableJoke, joke.toSqlite(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<int> remove(Joke joke) async {
    return await db!.delete(tableJoke,
        where: 'id = ? and favorite = ?', whereArgs: [joke.id, 1]);
  }

  Future<List<Joke>> getJokes() async {
    final data =
        await db!.query(tableJoke, where: 'favorite = ?', whereArgs: [1]);
    return data.map((e) => Joke.fromSqlite(e)).toList();
  }
}
