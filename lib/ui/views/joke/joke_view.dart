import 'package:flutter/material.dart';
import 'package:jokes/business_logic/view_models/joke_viewmodel.dart';
import 'package:jokes/ui/views/joke/widgets/joke_error.dart';
import 'package:jokes/ui/views/joke/widgets/joke_loading.dart';
import 'package:jokes/ui/widgets/jokes_scaffold.dart';
import 'package:provider/provider.dart';

import '../../../business_logic/view_models/favorite_viewmodel.dart';

class JokeView extends StatefulWidget {
  late final bool mode;
  late final int position;

  JokeView({Key? key, bool? mode, int? position}) : super(key: key) {
    this.mode = mode ?? false;
    this.position = position ?? 0;
  }

  @override
  State<JokeView> createState() => _JokeViewState();
}

class _JokeViewState extends State<JokeView> {
  Widget buildJokeSuccess(BuildContext context, JokeViewModel jokeViewModel) {
    final joke = jokeViewModel.joke;
    final height = MediaQuery.of(context).size.height;
    return JokesScaffold(
        body: SafeArea(
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.only(top: height * 0.1),
            child: Text(
              joke.category,
              style: TextStyle(fontSize: height * 0.02),
            ),
          ),
          Chip(
            label: Text(joke.safe ? 'Safe' : 'Not safe'),
          ),
          SizedBox(
            height: height * 0.1,
          ),
          SizedBox(
            height: height * 0.25,
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.only(left: 20, right: 20),
                child: Text(
                  joke.joke,
                  style: TextStyle(
                      fontSize: height * 0.03, fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
          SizedBox(
            height: height * 0.1,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                  onPressed: !jokeViewModel.isFirstJoke()
                      ? () {
                          jokeViewModel.fetchPreviousJoke();
                        }
                      : null,
                  child: const Icon(Icons.arrow_back)),
              ElevatedButton(
                  onPressed: () {
                    //jokeViewModel.editFavorite();
                    jokeViewModel.editFavorite(jokeViewModel.joke);
                  },
                  child: joke.favorite
                      ? const Icon(
                          Icons.favorite,
                          color: Colors.red,
                        )
                      : const Icon(Icons.favorite_border)),
              ElevatedButton(
                  onPressed: jokeViewModel.isLastJokeOfFavorites()
                      ? null
                      : () {
                          //jokeViewModel.nextJoke();
                          jokeViewModel.fetchNextJoke();
                        },
                  child: const Icon(Icons.arrow_forward)),
            ],
          ),
          SizedBox(
            height: height * 0.1,
          )
        ],
      ),
    ));
  }

  @override
  void initState() {
    Provider.of<JokeViewModel>(context, listen: false)
        .fetchJoke(true, widget.mode, widget.position);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        //Reset values in Provider before going back
        Provider.of<JokeViewModel>(context, listen: false).disposeValues();
        Navigator.pop(context, true);
        return true;
      },
      child: Consumer<JokeViewModel>(
        builder: (context, jokeViewModel, _) {
          switch (jokeViewModel.jokeStatus) {
            case JokeStatus.initial:
              //LoadJoke
              return const JokesScaffold(body: JokeLoading());
            case JokeStatus.loading:
              return const JokesScaffold(body: JokeLoading());
            case JokeStatus.failure:
              return const JokesScaffold(body: JokeError());
            case JokeStatus.success:
              return buildJokeSuccess(context, jokeViewModel);
          }
        },
      ),
    );
  }
}
