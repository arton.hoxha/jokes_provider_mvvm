import 'package:flutter/cupertino.dart';
import 'package:jokes/services/repository/joke_repository.dart';

import '../model/joke.dart';

enum JokeStatus { initial, loading, success, failure }

class JokeViewModel extends ChangeNotifier {
  JokeStatus jokeStatus;
  Joke joke;
  int _position;
  bool mode = false;
  final List<Joke> history;
  final List<Joke> favorites;
  final JokeRepository jokeRepository;

  JokeViewModel({required this.jokeRepository})
      : joke = Joke.empty,
        _position = 0,
        history = [],
        favorites = [],
        jokeStatus = JokeStatus.loading;

  void disposeValues() {
    joke = Joke.empty;
    _position = 0;
    history.clear();
    favorites.clear();
    mode = false;
    jokeStatus = JokeStatus.loading;
  }

  bool isFirstJoke() {
    return _position == 0;
  }

  bool isLastJokeOfFavorites() {
    return mode && _position == favorites.length - 1;
  }

  void fetchJoke(bool isSafeMode, bool mode, int position) async {
    //_setLoading();
    try {
      if (mode) {
        joke = await jokeRepository.getFavorites().then((value) {
          favorites.addAll(value);
          _position = position;
          return favorites.elementAt(position);
        });
      } else {
        joke = await jokeRepository.getJoke(isSafeMode);
        history.add(joke);
      }
      this.mode = mode;
      _setSuccess();
    } catch (e) {
      print(e);
      _setError();
    }
  }

  void editFavorite(Joke joke) async {
    joke.favorite = !joke.favorite;
    final res = await jokeRepository.editFavorite(joke);
    notifyListeners();
  }

  void fetchNextJoke() async {
    //setLoading
    _setLoading();
    try {
      if (mode) {
        if (_position < favorites.length - 1) {
          joke = favorites[++_position];
        }
      } else {
        if (_position == history.length - 1) {
          //We are at the end of history so we need to fetch a new joke
          joke = await jokeRepository.getJoke(true);
          _position++;
          history.add(joke);
        } else {
          //get from history
          _position++;
          joke = history[_position];
        }
      }
      _setSuccess();
    } catch (e) {
      print(e);
      _setError();
    }
  }

  void _setLoading() {
    if (jokeStatus != JokeStatus.loading) {
      jokeStatus = JokeStatus.loading;
      notifyListeners();
    }
  }

  void _setSuccess() {
    jokeStatus = JokeStatus.success;
    notifyListeners();
  }

  void _setError() {
    jokeStatus = JokeStatus.failure;
    notifyListeners();
  }

  void fetchPreviousJoke() async {
    if (isFirstJoke()) return;
    _position--;
    if (mode) {
      joke = favorites[_position];
    } else {
      joke = history[_position];
    }
    notifyListeners();
  }
}
