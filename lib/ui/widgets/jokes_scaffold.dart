import 'package:flutter/material.dart';
import 'package:jokes/business_logic/view_models/favorite_viewmodel.dart';
import 'package:jokes/ui/views/joke/joke_view.dart';
import 'package:provider/provider.dart';

class JokesScaffold extends StatefulWidget {
  const JokesScaffold({Key? key, required this.body}) : super(key: key);

  final Widget body;

  @override
  State<JokesScaffold> createState() => _JokesScaffoldState();
}

class _JokesScaffoldState extends State<JokesScaffold> {
  @override
  Widget build(BuildContext context) {
    final route = ModalRoute.of(context)?.settings.name;
    return Scaffold(
        appBar: AppBar(
          title: const Text('Jokes app'),
          actions: route != '/'
              ? null
              : [
                  IconButton(
                      icon: const Icon(Icons.add),
                      onPressed: () async {
                        final rebuild = await Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (_) => JokeView(
                                      mode: false,
                                    )));
                        if (rebuild) {
                          Provider.of<FavoriteViewModel>(context, listen: false)
                              .loadData();
                        }
                      })
                ],
        ),
        body: widget.body);
  }
}
